import React, { Component } from 'react';
import axios from "axios";
import PostDetails from './PostDetails';
import './App.css';

class App extends Component {

  state = {
    postUrl: "http://localhost:8080/api/posts/",
    posts: [],
    post: {}
  }

  getPosts() {
    axios.get(this.state.postUrl).then(response => {
      const data = response.data;
      this.setState({ posts:data });
    });
  }

  getPost(e) {
    axios.get(this.state.postUrl + e.id).then(response => {
      const data = response.data;
      this.setState({ post:data });
    });
  }

  componentDidMount() {
    this.getPosts();
  }

  render() {
    return (
      <div className="container">
        <h3>LITS мероприятия</h3>
        <ul>
        {
          this.state.posts.map(item => {
            return (
                <li key={item.id} onClick={() => this.getPost(item)}>{item.name}</li>
              );
          })
        }
        </ul>
        <PostDetails post={this.state.post}/>
      </div>
    );
  }
}

export default App;

import React, { Component } from 'react';

  export default class SubscriberDetails extends Component {

  render() {
    return (
      <li key={this.props.sub.url}>
        <a href={this.props.sub.url} target="_blank" rel="noopener noreferrer">{this.props.sub.name}</a>
        <p>Дата добавления: {this.props.sub.date}</p>
      </li>
    );
  }
}

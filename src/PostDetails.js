import React, { Component } from 'react';
import SubscriberDetails from './SubscriberDetails';

export default class PostDetails extends Component {

  render() {
    return (
      <div className="container">
        {
          this.props.post.subscribers !== undefined ?
          (
            <div>
              <h4>Детальная информация</h4>
              <h4><a href={this.props.post.url} target="_blank" rel="noopener noreferrer">{this.props.post.name}</a></h4>
              <p>{this.props.post.active !== undefined ? "Статус: " + this.props.post.active.toString() : ""}</p>
              <h5>Подпищики</h5>
              <ul>
              {
                this.props.post.subscribers.map(item => {
                  return (
                    <SubscriberDetails key={item.url} sub={item}/>
                  );
                })
              }
              </ul>
            </div>
          ) : (<ul></ul>)
        }
      </div>
    );
  }
}
